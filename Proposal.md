Fra http://www.java.no/submitit/index.html  

# Title
Getting started with Heroku (workshop)

# Abstract
Publish a simple web application on the internet using Heroku. The application is prepared, so we will concentrate on installing and using the Heroku command line tools. It is quick, free and relatively easy. Your application should be on the internet in less than 30 minutes, leaving time for some experimentation with an online datastore - either PostgreSql or MongoDb.  
You will not only learn how to publish web applications through Heroku. Having hands-on experience gives you a better understanding of cloud infrastructure and methodology. The 12-factor methodology for building software-as-a-service applications (http://12factor.net), and the means to mash up with other cloud-services such as mongoDb-hq through a rich addon environment.  
Heroku is the leading open language cloud application platform (PaaS) and supports Java, Scala, Python, Ruby, Clojure, Node.js. and custom language buildpacks.

# Language
English and Norwegian

# Level
Beginner

# Outline
5 minute introduction to Heroku: General information and a walk-through of the registration process.  
7 minutes to get set up: Attendees register and install the tools needed.  
7 minutes local test: Check out and run up the example application locally.  
5 minutes to publish: Use Heroku and git to publish the application and admire it.  
5 minutes wrap-up presentation: Explain set up for this application, and how to set up for other languages and frameworks using procfile.  
Optional excercises:  
5 minutes to make it your own: Connect a custom domain for those who have one.  
10 minutes to try datastore add-on: Attendees will add datastore to skeleton application, we use PostgreSql and/or MongoDb.  

# Highlight summary
Publish your own application in the cloud.

# Equipment
Classroom with network for attendees and video projector

# Expected audience
Anyone curious to start using a cloud platform. Basic programming skills required.

# Tags
+ Distributed systems and cloud 
+ Craftsmanship and Tools 

# Speaker 1
Name: Tommy Skodje  
Email: tsk@antares.no  
Speaker bio: Tommy Skodje is a senior systems consultant for Antares Gruppen AS. He prefers hands-on experience to powerpoint presentations and the skies to the ground. As a technical lead he has arranged for several different hands-on workshops and has been a driving force moving Antares Gruppen into the clouds.  
Picture: TommyAntares-Blid.jpg

# Speaker 2
Name: Håkon Sønderland  
Email: hts@antares.no  
Speaker bio: Håkon Sønderland is a senior systems consultant for Antares Gruppen AS who has developed software professionally since 1986. The last 10 years mostly developing java web applications.  
Picture: hts-122

# Speaker 3
Name: Stig Helge Strøm  
Email: shs@antares.no  
Speaker bio: Stig Helge Strøm is a senior systems consultant for Antares who has developed software professionally since 2000. Since 2006 mostly in Java. He is passionate about open source, building and automation.  
Picture: shs-097

