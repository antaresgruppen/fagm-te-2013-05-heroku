
# Prerequisites
Basic Java knowledge, including an installed version of the JVM and [Maven 3][mvn].  

A credit card is needed for bonus excercise with mongoDb.  
Your own domain-name is for bonus excercise where you connect one (+ login to your DNS hosting).  

**DOS:** you need administrator access while doing this workshop.  

Ambitious attendees have read [The twelve-factor app][12fact].  

Most interesting: [Dev/prod parity][devprod]  
[Vagrant][vagrant]  
[Debugging JVM Programs on Heroku][debug]  

Tip: If you did try Heroku earlier, use same user now.  
You want to avoid having several users for heroku (or git) on the same computer, lest you want to go through the hassle of managing ssh-keys. See [ref that saved me][2ssh] with comment by Tom Carchrae.  

# In to the cloud
## Register and install
"Sign up" on [Heroku][heroku].  
Click confirm link in the email you received.  
After submitting a password you will be taken to "Getting started".  
Install the "Heroku Toolbelt" (link on "Getting started").  
Try `heroku help` (command line) to see if all is well.  

The Heroku Toolbelt gives access to the [Heroku command-line client][herokucli], [Foreman][procfile], and the Git revision control system.

*15 minute mark*

## Local checkout and build

	:::term
	git clone https://bitbucket.org/antaresgruppen/fagm-te-2013-05-heroku.git
	cd path-to-project
	mvn package
	export PORT=5000
	(DOS: set PORT=5000)
	java -cp target/classes:"target/dependency/*" HelloWorld
	(DOS: java -cp target/classes;"target/dependency/*" HelloWorld)  

(double quotes needed to prevent expansion of `*`)  
See that it works locally (*http://localhost:5000/*).  
Stop application with SIGTERM (ctrl+c).  


## Pushing to Heroku
**DOS:** you must add git/bin to PATH (for ssh key generation).  
Choose to add ssh key, be wary of dropped network or other problems. If bad shit happens before first push, you may be served well to start with a clean slate (check out new version and continue below).  

	:::term
	heroku login
	heroku create
	git push heroku master
	heroku open

The above is first time only (when creating).  
Later, commit changes before pushing to Heroku:  

	:::term
	heroku login
	git commit -m 'message here'
	git push heroku master
	heroku open

If you need to see logs: `heroku logs`, or `heroku logs -t`  

*30 minute mark*

# What happened?

Heroku will:

  1. recognize the application as Java by the existence of pom.xml
  2. build it using Maven as build tool
  3. set the environment variable *PORT*, identifying the HTTP port to which the web process should bind (and some other variables).
  4. run the application with the command given in *procfile*

The application's *HelloWorld* class contains a *main* method that configures and starts up an embedded Jetty server. The welcome file is a static html file.  
What is the url to acess the servlet itself?  
Note that Heroku runs your application on the [OpenJDK](http://openjdk.java.net/) version 6 or 7 (8 available in beta).  

## Logging and debugging
There is a slight problem with CV-servlet, try url *http://<your server>/cv/bad* and check log from server.  

Look at the code written.  
How are errors logged?  
How should you log debug / info?  

Hint: request routing is found in *HelloWorld.main(...)*.  


# À la carte
Choose from below

## Custom Domain
Read and follow [about custom domains on Heroku][cname].  
Then use your DNS Service administrative interface to add a CNAME record that point the chosen name to your heroku application.  

## Postgres Addon
Read and follow [Heroku Postgres][heroku-postgres] to setup database.  
The code to use database is written in *Postgres.java*, except you need to pass uri to constructor.  

Hint: read through "Establish primary db".  
Hint: read also "Connecting in Java".  

## MongoHQ Addon
Before you can use MongoHQ addon, you need to "[verify][verify]" your account (enter credit card). According to [billing][billing], only addons heroku-postgresql:dev and pgbackups:plus are excempted.  

Verified? go on - read and follow [Heroku MongoHQ][mongohq] to setup database.  
The code to use database is written in *MongoDB.java*, except you need to pass uri to constructor.  

Hint: check table of contents, only 2 sections relevant from setup page.  


# Optional extras
For when you completed the above

## Run a worker process
The [Java workbook][javamore] has an excercise that shows how you can run a worker process. Remember to stop it before free dyno-hours tick away.  

## Run a [One-off dyno][oneoffdyno]
For example, use it to set up database.  
`heroku java -cp target/classes:"target/dependency/*" Postgres`. Or any other class with a *main(...)*.  

## Local access to Postgres or MongoHQ?
Snoop the connection url and see if you can get to cloud database from your own PC.  

## OAuth 2 [Google][gooauth]
Sharing done through external service.  


# Resources
This [workshop][fagm] is evolved from from the [Java demo application](https://github.com/heroku/devcenter-java) on GitHub.  

## Heroku and Java
[Introduction to Heroku for Java Developers][javaintro]  
[Java][javaheroku] on Heroku  
Heroku [Java devcenter][javadev]  
Getting [Started with Java on Heroku][javastart]  
Learn [more about Heroku for Java][javamore]

## Heroku
[Heroku dashboard][dash]
[Heroku addons][addons]  
[Applying the Unix Process Model to Web Apps][unixproc]  
[The twelve-factor app][12fact]  


## More sample code
PostgreSql [Hello World][hellopostgres], MongoDb [from Java][javamongo] and MongoDb [Hello World][hellomongo].  

[Handlebars.js][handlebars]  

-------------------------------------------------


  [heroku]: http://www.heroku.com  "Heroku site"
  [dash]: https://dashboard.heroku.com/apps  "Heroku dashboard"
  [herokucli]: http://devcenter.heroku.com/categories/command-line  "Heroku command-line client"
  [billing]: https://devcenter.heroku.com/categories/billing  "Heroku: billing"
  [verify]: https://devcenter.heroku.com/articles/account-verification  "Heroku: verify account"
  [oneoffdyno]: https://devcenter.heroku.com/articles/one-off-dynos  "Heroku: One-off dyno"
  [procfile]: https://devcenter.heroku.com/articles/procfile  "Heroku: procfile"

  [javaintro]: https://devcenter.heroku.com/articles/intro-for-java-developers  "Java intro"
  [javaheroku]: http://java.heroku.com  "Java on Heroku"
  [javadev]: https://devcenter.heroku.com/categories/java  "Java on Heroku devcenter"
  [javastart]: https://devcenter.heroku.com/articles/java  "Getting Started with Java on Heroku"
  [javamore]: https://devcenter.heroku.com/articles/java-learn-more  "Learn more about Heroku for Java"
  [mongohq]: https://devcenter.heroku.com/articles/mongohq  "MongoHQ addon"
  [hellomongo]: http://www.mkyong.com/mongodb/java-mongodb-hello-world-example/  "MongoDb Hello World"
  [javamongo]: http://docs.mongodb.org/ecosystem/tutorial/getting-started-with-java-driver/  "Getting started with MongoDB Java"
  [postgresql]: https://addons.heroku.com/heroku-postgresql  "postgresql addon"
  [heroku-postgres]: https://devcenter.heroku.com/articles/heroku-postgresql  "Heroku Postgres doc"
  [hellopostgres]: https://github.com/heroku/devcenter-java-database  "postgresql Hello World"
  [neo4j]: https://devcenter.heroku.com/articles/neo4j  "neo4j addon"
  [cname]: https://devcenter.heroku.com/articles/custom-domains  "About custom domains"
  [gooauth]: https://developers.google.com/accounts/docs/OAuth2  "Google OAuth 2"

  [addons]: https://addons.heroku.com/  "Heroku addons"
  [unixproc]: http://adam.heroku.com/past/2011/5/9/applying_the_unix_process_model_to_web_apps/  "Blog post on unix process model"
  [12fact]: http://www.12factor.net/  "The twelve-factor app"
  [devprod]: http://www.12factor.net/dev-prod-parity  "Dev/prod parity"
  [vagrant]: http://www.vagrantup.com/  "vagrant site"
  [debug]: http://mikeslinn.blogspot.no/2012/09/debugging-jvm-programs-on-heroku.html  "Debugging JVM Programs on Heroku"

  [handlebars]: http://handlebarsjs.com/  "handlebars js"
  [mustache]: http://mustache.github.com/  "mustache js"

  [fagm]: https://bitbucket.org/antaresgruppen/fagm-te-2013-05-heroku  "Hands-on instructions"
  [mvn]: http://maven.apache.org/download.html  "Maven download"

  [ssh]: http://stackoverflow.com/questions/11440683/heroku-key-not-accepted  "ssh key not accepted"
  [herssh]: https://devcenter.heroku.com/articles/keys  "Heroku ssh keys"
  [2ssh]: http://stackoverflow.com/questions/8786564/cannot-push-to-heroku-because-key-fingerprint  "Post that saved my assh"
  []:   ""


