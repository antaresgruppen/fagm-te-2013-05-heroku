import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoURI;
import com.mongodb.util.JSON;

public class MongoDb extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// not thread-safe, but will do for a demo
	private final DB mongoDb; 

	MongoDb( String dbUri ) {
		System.out.println( "Initializing MongoDB at " + dbUri );
		mongoDb	= connect( dbUri );
		if ( mongoDb != null ) {
			Set<String> colls	= mongoDb.getCollectionNames();
			save( CV.tsk );
			System.out.println("Collections found in DB: " + colls.toString());
		}
	}

	@Override protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id	= req.getPathInfo().substring( 1 );
		String cv	= StringUtils.join( find( id ), "\n" );
		resp.getWriter().print( cv );
	}

	/** */
	private List<String> find( String id ) {
		if ( mongoDb == null )
			return Arrays.asList( "{ 'message': 'No database connected' }" );
		DBCollection collection = mongoDb.getCollection( "cvs" );

		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put( "_id", id );
		DBCursor cursor = collection.find( searchQuery );

		List<String> result	= new ArrayList<>();
		while ( cursor.hasNext() ) {
			result.add( cursor.next().toString() );
		}

		return result;
	}

	private void save( String... cvs ) {
		if ( mongoDb == null )
			return;
		DBCollection collection = mongoDb.getCollection( "cvs" );
		for ( String json : cvs ) {
			DBObject dbObject = (DBObject) JSON.parse( json );
			collection.save(  dbObject );
		}
	}

	private DB connect( String uri ) {
		if ( StringUtils.isEmpty( uri ) )
			return null;
		try {
			System.out.println( "Connecting to: " + uri );
			MongoURI mongoURI = new MongoURI( uri );
			DB db = mongoURI.connectDB();
			db.authenticate( mongoURI.getUsername(), mongoURI.getPassword() );
			return db;
		} catch ( Throwable t ) {
			System.err.println( "Exception initializing MongoDB: " + t.getMessage() );
			t.printStackTrace( System.err );
			return null;
		}
	}

}
