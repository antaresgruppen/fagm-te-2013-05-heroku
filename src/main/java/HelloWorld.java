import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class HelloWorld extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String FIXME	= null;

	/** Answer request */
	@Override protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.getWriter().print( "Hello world!\n From servlet\n" );
	}

	/** Set up and start Jetty embedded */
	public static void main(String[] args) throws Exception {
		try {
			int port	= Integer.valueOf( System.getenv( "PORT" ) );
			Server server = new Server( port );
			ServletContextHandler context = new ServletContextHandler( ServletContextHandler.SESSIONS );
			context.setContextPath( "/" );

			// Serve servlets
	    HandlerList handlers = new HandlerList();
			context.addServlet( new ServletHolder( new HelloWorld() ) ,"/hello" );
			context.addServlet( new ServletHolder( new CV() ) ,"/cv/*" );
			context.addServlet( new ServletHolder( new MongoDb( FIXME ) ) ,"/mongodb/*" );
			context.addServlet( new ServletHolder( new Postgres( FIXME ) ) ,"/postgres" );
			
			// Serve static resources cf. http://stackoverflow.com/questions/10284584/serving-static-files-w-embedded-jetty
	    ResourceHandler resource_handler = new ResourceHandler();
	    resource_handler.setDirectoriesListed( true );
	    resource_handler.setWelcomeFiles( new String[]{ "index.html" } );
	    resource_handler.setResourceBase( "src/main/webapp/" );

	    handlers.setHandlers( new Handler[] { resource_handler, context, new DefaultHandler() } );
	    server.setHandler( handlers );
	
	    server.start();
			server.join();   
		} catch ( Throwable t ) {
			System.err.println( "Exception initializing Server: " + t.getMessage() );
			t.printStackTrace( System.err );
			throw t;
		}
	}
}
