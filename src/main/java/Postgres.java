import java.io.IOException;
import java.net.URI;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

/** from https://github.com/heroku/devcenter-java-database */
public class Postgres extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// not thread-safe, but will do for a demo
	private final Connection db;

	Postgres( String dbUri ) {
		System.out.println("Initializing PostgreSql at " + dbUri);
  	db	= connect( dbUri );
  	createDataBase();
	}

	@Override protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
  	StringBuilder sb	= new StringBuilder( "Hello world!\n From PostgreSql servlet\n" );
  	sb.append( StringUtils.join( find(), "\n" ) );
		resp.getWriter().print( sb.toString() );
		tick();
	}

	private List<String> find() {
		if ( db == null )
			return Arrays.asList(  "No database connected" );
  	Statement stmt = null;
  	ResultSet rs = null;
  	List<String> result	= new ArrayList<>();
		try {
	    stmt = db.createStatement();
	    rs = stmt.executeQuery( "SELECT tick FROM ticks" );
	    result.add( "Read ticks from DB:" );
	    while ( rs.next() ) {
	    	result.add( rs.getTimestamp( "tick" ).toString() );
	    }
		} catch ( Throwable t ) {
			System.err.println( "Exception querying DB: " + t.getMessage() );
			t.printStackTrace( System.err );
		} finally {
			close( rs );
			close( stmt );
		}
    return result;
  }

	private void tick() {
		if ( db == null )
			return;
  	Statement stmt = null;
		try {
	    stmt = db.createStatement();
	    stmt.executeUpdate( "INSERT INTO ticks VALUES (now())" );
		} catch ( Throwable t ) {
			System.err.println( "Exception inserting in DB: " + t.getMessage() );
			t.printStackTrace( System.err );
		} finally {
			close( stmt );
		}
  }

	private void createDataBase() {
		if ( db == null )
			return;
  	Statement stmt = null;
		try {
	    stmt = db.createStatement();
	    // stmt.executeUpdate( "DROP TABLE IF EXISTS ticks" );
	    stmt.executeUpdate( "CREATE TABLE ticks (tick timestamp)" );
		} catch ( Throwable t ) {
			System.err.println( "Exception initializing DB: " + t.getMessage() );
			t.printStackTrace( System.err );
		} finally {
			close( stmt );
		}
  }

	private Connection connect( String uri ) {
		if ( uri == null )
			return null;
		try {
			System.out.println( "Connecting to: " + uri );
	    URI dbUri = new URI( uri );

	    String username = dbUri.getUserInfo().split(":")[0];
	    String password = dbUri.getUserInfo().split(":")[1];
	    String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath();

	    return DriverManager.getConnection( dbUrl, username, password );
		} catch ( Throwable t ) {
			System.err.println( "Exception connecting to DB: " + t.getMessage() );
			t.printStackTrace( System.err );
			return null;
		}
	}

  private static void close( Statement stmt ) {
		try {
			if ( stmt != null )
				stmt.close();
		} catch ( Throwable t ) {
		}
	}

	private static void close( ResultSet rs ) {
		try {
			if ( rs != null )
				rs.close();
		} catch ( Throwable t ) {
		}
	}

}
