import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;


public class CV extends HttpServlet {
	private static final long serialVersionUID = 1L;

	static final String unknown =
			"{	'_id' : 'unknown'," +
			"		'fn' : 'John', " +
			"		'ln' : 'Doe', " +
		  "		'key' : { " +
			"			'db' : [ " +
		  " 	}" +
			"}"
		;

	static final String tsk =
			"{	'_id' : 'tsk'," +
			"		'fn' : 'Tommy', " +
			"		'ln' : 'Skodje', " +
		  "		'key' : { " +
			"			'cloud' : [ 'Heroku', 'Google App Engine' ], " +
			"			'lang' : [ 'Java', 'Scala', 'C', 'C++', 'Objective-C', 'Clojure', 'Pascal', 'Basic', 'Fortran' ], " +
			"			'db' : [ 'sql', 'mongoDb', 'jdbc' ] " +
		  " 	}" +
			"}"
		;

	static final Map<String,String> cvs	= new HashMap<>();
	static {
		cvs.put( "tsk", tsk );
		cvs.put( "bad", unknown );
	}

	@Override protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id	= req.getPathInfo().substring( 1 );

		String cv	= unknown;
		if ( cvs.containsKey( id ) )
			cv	= cvs.get( id );

		resp.setContentType("application/json");
    resp.setHeader("Cache-Control", "no-cache");
		PrintWriter out = resp.getWriter();
		try {
			JSONObject json	= new JSONObject( cv );
			json.write( out );
		} catch ( JSONException e ) {
			System.err.println( "Parse error: " + e.getMessage() + "\nJson:\n" + cv );
			out.write( "{}" );
		}
		out.flush();
	}

}
